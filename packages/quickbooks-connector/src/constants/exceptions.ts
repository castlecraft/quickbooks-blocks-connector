import { HttpException, HttpStatus } from '@nestjs/common';
import { SETUP_ALREADY_COMPLETE } from './messages';

export const settingsAlreadyExists = new HttpException(
  SETUP_ALREADY_COMPLETE,
  HttpStatus.BAD_REQUEST,
);
