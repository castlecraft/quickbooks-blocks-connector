export const ADMINISTRATOR = 'administrator';
export const TOKEN = 'token';
export const AUTHORIZATION = 'authorization';
export const SERVICE = 'quickbooks-connector';
export const PUBLIC = 'public';
export const APP_NAME = 'quickbooks-connector';
export const SWAGGER_ROUTE = 'api-docs';
export const COMMUNICATION_SERVER = 'communication-server';
export enum ConnectedServices {
  CommunicationServer = 'communication-server',
  InfrastructureConsole = 'infrastructure-console',
  IdentityProvider = 'identity-provider',
}
export const BEARER_HEADER_VALUE_PREFIX = 'Bearer';
export const APPLICATION_JSON_CONTENT_TYPE = 'application/json';
export const CONTENT_TYPE_HEADER_KEY = 'content-type';
export const ACCEPT_HEADER_KEY = 'accept';
export const TWENTY_MINUTES_IN_SECONDS = 20 * 60; // 20 min * 60 sec;
export const TWENTY_FOUR_HOURS_IN_MINUTES = 1440;
export const SWAGGER_SCHEME = 'https';
export const THIRTY = 30;
export enum QuickBooksEnvironment {
  Staging = 'staging',
  Production = 'production',
}
export const APPLICATION_X_WWW_FORM_URLENCODED =
  'application/x-www-form-urlencoded';
