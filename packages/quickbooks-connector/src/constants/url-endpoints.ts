export const INFO_ENDPOINT = '/info';
export const SERVICE_TYPE_CREATE_ENDPOINT = '/service_type/v1/create';
export const SERVICE_REGISTER_ENDPOINT = '/service/v1/register';
export const QUICKBOOKS_PRODUCTION_OIDC_CONFIG =
  'https://developer.api.intuit.com/.well-known/openid_configuration';
export const QUICKBOOKS_STAGING_OIDC_CONFIG =
  'https://developer.api.intuit.com/.well-known/openid_sandbox_configuration';
export const QUICKBOOKS_AUTHORIZATION_URL =
  'https://appcenter.intuit.com/connect/oauth2';
export const QUICKBOOKS_TOKEN_URL =
  'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer';
export const QUICKBOOKS_REVOCATION_URL =
  'https://developer.api.intuit.com/v2/oauth2/tokens/revoke';
export const QUICKBOOKS_SANDBOX_URL =
  'https://sandbox-quickbooks.api.intuit.com/v3/company';
export const QUICKBOOKS_PRODUCTION_URL =
  'https://quickbooks.api.intuit.com/v3/company';
