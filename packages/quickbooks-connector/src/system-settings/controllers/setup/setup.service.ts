import { Injectable, HttpService } from '@nestjs/common';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { settingsAlreadyExists } from '../../../constants/exceptions';
import { ServerSettingsService } from '../../entities/server-settings/server-settings.service';
import {
  COMMUNICATION_SERVER,
  TWENTY_FOUR_HOURS_IN_MINUTES,
} from '../../../constants/app-strings';
import { ServerSettings } from '../../entities/server-settings/server-settings.entity';

@Injectable()
export class SetupService {
  setupData: { [key: string]: any } = {};
  constructor(
    protected readonly serverSettingsService: ServerSettingsService,
    protected readonly http: HttpService,
  ) {}

  async setup(params) {
    if (await this.serverSettingsService.count()) {
      throw settingsAlreadyExists;
    }

    const settings = new ServerSettings();
    Object.assign(settings, params);
    await settings.save();

    this.http
      .get(params.authServerURL + '/.well-known/openid-configuration')
      .pipe(
        switchMap(openIdConf => {
          this.setupData = openIdConf.data;
          return this.http.get(params.authServerURL + '/info');
        }),
        switchMap(authInfo => {
          const communicationService = authInfo.data.services.filter(
            service => service.type === COMMUNICATION_SERVER,
          );
          if (communicationService.length > 0) {
            this.setupData.communicationService = communicationService[0].url;
          }
          return of(this.setupData);
        }),
      )
      .subscribe({
        next: async response => {
          params.authorizationURL = response.authorization_endpoint;
          params.tokenURL = response.token_endpoint;
          params.profileURL = response.userinfo_endpoint;
          params.revocationURL = response.revocation_endpoint;
          params.introspectionURL = response.introspection_endpoint;
          params.callbackURLs = [
            params.appURL + '/index.html',
            params.appURL + '/silent-refresh.html',
          ];
          params.communicationService = response.communicationService;
          params.expireRequestLogInMinutes = TWENTY_FOUR_HOURS_IN_MINUTES;
          Object.assign(settings, params);
          await settings.save();
        },
        error: async error => await settings.remove(),
      });
  }

  async getInfo() {
    const info = await this.serverSettingsService.find();
    if (info) {
      delete info.clientSecret, info._id;
    }
    return info;
  }
}
