import {
  IsUrl,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  Min,
  IsString,
} from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { THIRTY } from '../../../constants/app-strings';

export class ServerSettingsDto {
  uuid?: string;

  @IsUrl()
  @ApiModelProperty({
    description: 'The URL of the server.',
    type: 'string',
    required: true,
  })
  appURL: string;

  @IsUrl()
  @ApiModelProperty({
    description: 'The URL of the authorization server.',
    type: 'string',
    required: true,
  })
  authServerURL: string;

  @IsNotEmpty()
  @ApiModelProperty({
    description: 'client_id registered on authorization server',
    type: 'string',
    required: true,
  })
  clientId: string;

  @IsNotEmpty()
  @ApiModelProperty({
    description:
      'client_secret for the client registered on authorization server.',
    type: 'string',
    required: true,
  })
  clientSecret: string;

  @IsNumber()
  @Min(THIRTY)
  @IsOptional()
  @ApiModelProperty({
    description: 'Number of minutes after Request Logs expire',
    default: 1440,
    type: 'number',
    required: false,
  })
  expireRequestLogInMinutes: number;

  @IsUrl({ allow_underscores: true }, { each: true })
  callbackURLs: string[];

  @IsString()
  @IsOptional()
  callbackProtocol: string;
}
