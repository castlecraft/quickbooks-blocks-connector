export enum QuickBooksEnvironment {
  SANDBOX = 'sandbox',
  PRODUCTION = 'production',
}
