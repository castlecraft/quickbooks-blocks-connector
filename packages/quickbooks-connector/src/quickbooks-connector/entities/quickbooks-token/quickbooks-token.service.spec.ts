import { Test, TestingModule } from '@nestjs/testing';
import { QuickBooksTokenService } from './quickbooks-token.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { QuickBooksToken } from './quickbooks-token.entity';

describe('QuickBooksTokenService', () => {
  let service: QuickBooksTokenService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuickBooksTokenService,
        { provide: getRepositoryToken(QuickBooksToken), useValue: {} },
      ],
    }).compile();

    service = module.get<QuickBooksTokenService>(QuickBooksTokenService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
