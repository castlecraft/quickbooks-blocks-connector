import { Module, HttpModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QuickBooksController } from './controllers/quickbooks/quickbooks.controller';
import { QuickBooksTokenManagerService } from './aggregates/quickbooks-token-manager/quickbooks-token-manager.service';
import { QuickBooksTokenService } from './entities/quickbooks-token/quickbooks-token.service';
import { QuickBooksToken } from './entities/quickbooks-token/quickbooks-token.entity';
import { RequestStateService } from './entities/request-state/request-state.service';
import { RequestState } from './entities/request-state/request-state.entity';
import { QuickBooksClientService } from './entities/quickbooks-client/quickbooks-client.service';
import { QuickBooksClient } from './entities/quickbooks-client/quickbooks-client.entity';
import { QuickBooksClientAggregateService } from './aggregates/quickbooks-client-aggregate/quickbooks-client-aggregate.service';
import { QuickBooksCommandHandlers } from './commands';
import { QuickBooksEventHandlers } from './events';
import { QuickBooksQueryHandlers } from './queries';
import { CqrsModule } from '@nestjs/cqrs';
import { RequestLogService } from './entities/request-log/request-log.service';
import { RequestLog } from './entities/request-log/request-log.entity';
import { RequestLogController } from './controllers/request-log/request-log.controller';
import { RequestLogManagerService } from './aggregates/request-log-manager/request-log-manager.service';
import { RequestLogCleanUpService } from './schedulers/request-log-clean-up/request-log-clean-up.service';
import { QuickBooksConnectionService } from './aggregates/quick-books-connection/quick-books-connection.service';

@Module({
  imports: [
    CqrsModule,
    HttpModule,
    TypeOrmModule.forFeature([
      QuickBooksToken,
      RequestState,
      QuickBooksClient,
      RequestLog,
    ]),
  ],
  controllers: [QuickBooksController, RequestLogController],
  providers: [
    // Aggregates
    QuickBooksTokenManagerService,
    QuickBooksClientAggregateService,
    RequestLogManagerService,
    QuickBooksConnectionService,

    // Entity Repositories
    QuickBooksTokenService,
    RequestStateService,
    QuickBooksClientService,
    RequestLogService,

    // CQRS
    ...QuickBooksCommandHandlers,
    ...QuickBooksEventHandlers,
    ...QuickBooksQueryHandlers,

    // Scheduler
    RequestLogCleanUpService,
  ],
})
export class QuickBooksConnectorModule {}
