import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { QuickBooksClientUpdatedHandler } from './quickbooks-client-updated.handler';
import { QuickBooksClientUpdatedEvent } from './quickbooks-client-updated.event';
import { QuickBooksClient } from '../../entities/quickbooks-client/quickbooks-client.entity';

describe('Event: QuickBooksClientUpdatedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: QuickBooksClientUpdatedHandler;
  const mockProvider = new QuickBooksClient();

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        QuickBooksClientUpdatedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<QuickBooksClientUpdatedHandler>(
      QuickBooksClientUpdatedHandler,
    );
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should save OAuth2Provider', async () => {
    eventBus$.publish = jest.fn(() => {});
    mockProvider.save = jest.fn(() => Promise.resolve(mockProvider));
    await eventHandler.handle(new QuickBooksClientUpdatedEvent(mockProvider));
    expect(mockProvider.save).toHaveBeenCalledTimes(1);
  });
});
