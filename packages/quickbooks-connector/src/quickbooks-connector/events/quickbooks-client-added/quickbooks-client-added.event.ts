import { IEvent } from '@nestjs/cqrs';
import { QuickBooksClient } from '../../entities/quickbooks-client/quickbooks-client.entity';

export class QuickBooksClientAddedEvent implements IEvent {
  constructor(public readonly client: QuickBooksClient) {}
}
