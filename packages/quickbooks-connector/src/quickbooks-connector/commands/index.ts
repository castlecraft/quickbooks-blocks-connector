import { AddQuickBooksClientHandler } from './add-quickbooks-client/add-quickbooks-client.handler';
import { RemoveQuickBooksClientHandler } from './remove-quickbooks-client/remove-quickbooks-client.handler';
import { UpdateQuickBooksClientHandler } from './update-quickbooks-client/update-quickbooks-client.handler';
import { MakeQuickBooksRequestHandler } from './make-quickbooks-request/make-quickbooks-request.handler';
import { RefreshClientTokenHandler } from './refresh-client-token/refresh-client-token.handler';

export const QuickBooksCommandHandlers = [
  AddQuickBooksClientHandler,
  RemoveQuickBooksClientHandler,
  UpdateQuickBooksClientHandler,
  MakeQuickBooksRequestHandler,
  RefreshClientTokenHandler,
];
