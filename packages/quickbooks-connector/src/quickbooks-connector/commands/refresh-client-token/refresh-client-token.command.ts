import { ICommand } from '@nestjs/cqrs';

export class RefreshClientTokenCommand implements ICommand {
  constructor(
    public readonly providerUuid: string,
    public readonly userUuid: string,
    public readonly realmId: string,
  ) {}
}
