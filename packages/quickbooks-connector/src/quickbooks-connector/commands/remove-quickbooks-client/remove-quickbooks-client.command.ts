import { ICommand } from '@nestjs/cqrs';

export class RemoveQuickBooksClientCommand implements ICommand {
  constructor(
    public readonly providerUuid: string,
    public readonly actorUuid: string,
  ) {}
}
