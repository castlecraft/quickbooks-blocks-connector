import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveQuickBooksClientCommand } from './remove-quickbooks-client.command';
import { QuickBooksClientAggregateService } from '../../aggregates/quickbooks-client-aggregate/quickbooks-client-aggregate.service';

@CommandHandler(RemoveQuickBooksClientCommand)
export class RemoveQuickBooksClientHandler
  implements ICommandHandler<RemoveQuickBooksClientCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: QuickBooksClientAggregateService,
  ) {}

  async execute(command: RemoveQuickBooksClientCommand) {
    // TODO: record actorUuid from command
    const { providerUuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.removeProvider(providerUuid);
    aggregate.commit();
  }
}
