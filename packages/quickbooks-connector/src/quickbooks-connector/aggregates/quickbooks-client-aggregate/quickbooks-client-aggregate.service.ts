import {
  Injectable,
  NotFoundException,
  HttpService,
  BadRequestException,
  UnauthorizedException,
  ForbiddenException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { stringify } from 'querystring';
import { switchMap, map, catchError } from 'rxjs/operators';
import { from, throwError } from 'rxjs';
import * as uuidv4 from 'uuid/v4';
import { QuickBooksClientDto } from '../../policies/quickbooks-client-dto/quickbooks-client.dto';
import { QuickBooksClientRemovedEvent } from '../../events/quickbooks-client-removed/quickbooks-client-removed.event';
import { QuickBooksClientService } from '../../entities/quickbooks-client/quickbooks-client.service';
import { QuickBooksClient } from '../../entities/quickbooks-client/quickbooks-client.entity';
import { QuickBooksTokenService } from '../../entities/quickbooks-token/quickbooks-token.service';
import { HttpMethod } from '../../../constants/request-methods';
import {
  INVALID_QUICKBOOKS_CLIENT,
  INVALID_QUICKBOOKS_TOKEN,
} from '../../../constants/messages';
import { QuickBooksToken } from '../../entities/quickbooks-token/quickbooks-token.entity';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { RequestLog } from '../../entities/request-log/request-log.entity';
import {
  CONTENT_TYPE_HEADER_KEY,
  AUTHORIZATION,
  BEARER_HEADER_VALUE_PREFIX,
  ACCEPT_HEADER_KEY,
  TWENTY_MINUTES_IN_SECONDS,
  TWENTY_FOUR_HOURS_IN_MINUTES,
  APPLICATION_JSON_CONTENT_TYPE,
  APPLICATION_X_WWW_FORM_URLENCODED,
} from '../../../constants/app-strings';
import { QuickBooksClientUpdatedEvent } from '../../events/quickbooks-client-updated/quickbooks-client-updated.event';
import { QuickBooksClientAddedEvent } from '../../events/quickbooks-client-added/quickbooks-client-added.event';
import { QuickBooksConnectionService } from '../quick-books-connection/quick-books-connection.service';
import { QuickBooksEnvironment } from '../../entities/quickbooks-client/quickbooks-client-environment.enum';

@Injectable()
export class QuickBooksClientAggregateService extends AggregateRoot {
  constructor(
    private readonly client: QuickBooksClientService,
    private readonly token: QuickBooksTokenService,
    private readonly http: HttpService,
    private readonly settings: ServerSettingsService,
    private readonly connection: QuickBooksConnectionService,
  ) {
    super();
  }

  async addProvider(payload: QuickBooksClientDto) {
    const provider = Object.assign(new QuickBooksClient(), payload);
    this.apply(new QuickBooksClientAddedEvent(provider));
  }

  async removeProvider(uuid: string) {
    const provider = await this.client.findOne({ uuid });
    if (!provider) throw new NotFoundException({ uuid });
    this.apply(new QuickBooksClientRemovedEvent(provider));
  }

  async retrieveProvider(uuid: string) {
    const provider = await this.client.findOne({ uuid });
    if (!provider) throw new NotFoundException({ uuid });
    return provider;
  }

  async updateProvider(uuid: string, payload: QuickBooksClientDto) {
    const provider = await this.client.findOne({ uuid });
    if (!provider) throw new NotFoundException({ uuid });
    const updatedProvider = Object.assign(provider, payload);
    this.apply(new QuickBooksClientUpdatedEvent(updatedProvider));
  }

  async list(offset: number, limit: number, search?: string, sort?: string) {
    offset = Number(offset);
    limit = Number(offset);
    return this.client.list(offset, limit, search, sort || 'ASC');
  }

  async makeRequest(
    reqHeaders: any,
    method: HttpMethod,
    clientUuid: string,
    params: any,
    query: any,
    payload: any,
    userUuid: string,
    paramString: string,
  ) {
    // parse realmId from uri params
    let realmId = paramString;
    if (paramString.split('/').length > 0) {
      realmId = paramString.split('/')[0];
    }

    const client = await this.client.findOne({ uuid: clientUuid });
    if (!client) throw new BadRequestException(INVALID_QUICKBOOKS_CLIENT);
    let token = await this.token.findOne({
      providerUuid: clientUuid,
      userUuid,
      realmId,
    });
    if (!token) throw new UnauthorizedException(INVALID_QUICKBOOKS_TOKEN);
    const settings = await this.settings.find();
    const expiresIn =
      settings.expireRequestLogInMinutes || TWENTY_FOUR_HOURS_IN_MINUTES;
    const expiration = new Date();
    expiration.setMinutes(expiration.getMinutes() + expiresIn);

    const requestLog = new RequestLog();
    requestLog.uuid = uuidv4();
    requestLog.userUuid = userUuid;
    requestLog.providerUuid = clientUuid;
    requestLog.expiration = expiration;
    requestLog.creation = new Date();

    await requestLog.save();

    token = await this.getUserAccessToken(token, requestLog);

    let requestUrl =
      this.connection.getBaseUrl(
        client.environment || QuickBooksEnvironment.SANDBOX,
      ) + `/${params[0]}`;

    if (Object.keys(query).length > 0) {
      requestUrl += `?${stringify(query)}`;
    }

    const requestSubscription = {
      next: async success => {
        requestLog.successResponse = success.data;
        await requestLog.save();
      },
      error: async error => {
        requestLog.failResponse = error.message;
        await requestLog.save();
      },
    };

    const catchInvalidTokenError = catchError(error => {
      try {
        if (error.response.data.exc.includes('validate_oauth')) {
          token
            .remove()
            .then(res => {})
            .catch(err => {});
        }
      } catch (error) {}
      return throwError(error);
    });

    const headers: any = {};

    headers[AUTHORIZATION] =
      BEARER_HEADER_VALUE_PREFIX + ' ' + token.accessToken;

    if (reqHeaders[CONTENT_TYPE_HEADER_KEY]) {
      headers[CONTENT_TYPE_HEADER_KEY] = reqHeaders[CONTENT_TYPE_HEADER_KEY];
    }

    if (reqHeaders[ACCEPT_HEADER_KEY]) {
      headers[ACCEPT_HEADER_KEY] = reqHeaders[ACCEPT_HEADER_KEY];
    }

    switch (method) {
      case HttpMethod.GET:
        this.http
          .get(requestUrl, { headers })
          .pipe(catchInvalidTokenError)
          .subscribe(requestSubscription);
        break;

      case HttpMethod.POST:
        this.http
          .post(requestUrl, payload, { headers })
          .pipe(catchInvalidTokenError)
          .subscribe(requestSubscription);
        break;

      case HttpMethod.PUT:
        this.http
          .put(requestUrl, payload, { headers })
          .pipe(catchInvalidTokenError)
          .subscribe(requestSubscription);
        break;

      case HttpMethod.PATCH:
        this.http
          .patch(requestUrl, payload, { headers })
          .pipe(catchInvalidTokenError)
          .subscribe(requestSubscription);
        break;

      case HttpMethod.DELETE:
        this.http
          .delete(requestUrl, { headers })
          .pipe(catchInvalidTokenError)
          .subscribe(requestSubscription);
        break;
    }

    return { requestLog: requestLog.uuid };
  }

  async getUserAccessToken(token: QuickBooksToken, requestLog: RequestLog) {
    const expiration = token.expirationTime;
    expiration.setSeconds(expiration.getSeconds() - TWENTY_MINUTES_IN_SECONDS);
    if (new Date() > expiration) {
      return this.refreshToken(token);
    } else {
      return token;
    }
  }

  revokeToken(accessToken: string, client: QuickBooksClient) {
    const headers = {};
    headers[ACCEPT_HEADER_KEY] = APPLICATION_JSON_CONTENT_TYPE;
    headers[CONTENT_TYPE_HEADER_KEY] = APPLICATION_JSON_CONTENT_TYPE;

    this.http
      .post(
        this.connection.getRevocationUrl(),
        { token: accessToken },
        {
          headers,
          auth: { username: client.clientId, password: client.clientSecret },
        },
      )
      .subscribe({
        next: success => {},
        error: error => {},
      });
  }

  async refreshToken(token: QuickBooksToken, requestLog?: RequestLog) {
    const headers = {};
    headers[ACCEPT_HEADER_KEY] = APPLICATION_JSON_CONTENT_TYPE;
    headers[CONTENT_TYPE_HEADER_KEY] = APPLICATION_X_WWW_FORM_URLENCODED;

    const client = await this.client.findOne({
      uuid: token.providerUuid,
    });
    const requestBody = {
      grant_type: 'refresh_token',
      refresh_token: token.refreshToken,
    };
    return this.http
      .post(this.connection.getTokenUrl(), stringify(requestBody), {
        headers,
        auth: { username: client.clientId, password: client.clientSecret },
      })
      .pipe(
        map(res => res.data),
        catchError(err => {
          if (requestLog) {
            requestLog
              .remove()
              .then(success => {})
              .catch(error => {});
          }

          token
            .remove()
            .then(success => {})
            .catch(error => {});

          return throwError(new ForbiddenException(INVALID_QUICKBOOKS_TOKEN));
        }),
        switchMap(bearerToken => {
          token.accessToken = bearerToken.access_token;
          token.refreshToken = bearerToken.refresh_token;

          const expirationTime = new Date();
          expirationTime.setSeconds(
            expirationTime.getSeconds() + (bearerToken.expires_in || 3600),
          );

          token.expirationTime = expirationTime;
          return from(token.save());
        }),
      )
      .toPromise();
  }

  async findOneToken(providerUuid: string, userUuid: string, realmId: string) {
    return await this.token.findOne({ providerUuid, userUuid, realmId });
  }
}
