import { Test, TestingModule } from '@nestjs/testing';
import { HttpModule } from '@nestjs/common';
import { QuickBooksTokenManagerService } from './quickbooks-token-manager.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { RequestStateService } from '../../entities/request-state/request-state.service';
import { QuickBooksTokenService } from '../../entities/quickbooks-token/quickbooks-token.service';
import { QuickBooksClientService } from '../../entities/quickbooks-client/quickbooks-client.service';
import { QuickBooksConnectionService } from '../quick-books-connection/quick-books-connection.service';

describe('QuickBooksTokenManagerService', () => {
  let service: QuickBooksTokenManagerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        QuickBooksTokenManagerService,
        { provide: SettingsService, useValue: {} },
        { provide: RequestStateService, useValue: {} },
        { provide: QuickBooksTokenService, useValue: {} },
        { provide: QuickBooksClientService, useValue: {} },
        { provide: QuickBooksConnectionService, useValue: {} },
      ],
    }).compile();

    service = module.get<QuickBooksTokenManagerService>(
      QuickBooksTokenManagerService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
