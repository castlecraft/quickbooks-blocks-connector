import { Injectable, HttpService } from '@nestjs/common';
import {
  QUICKBOOKS_STAGING_OIDC_CONFIG,
  QUICKBOOKS_PRODUCTION_OIDC_CONFIG,
  QUICKBOOKS_AUTHORIZATION_URL,
  QUICKBOOKS_TOKEN_URL,
  QUICKBOOKS_REVOCATION_URL,
  QUICKBOOKS_PRODUCTION_URL,
  QUICKBOOKS_SANDBOX_URL,
} from '../../../constants/url-endpoints';
import { QuickBooksEnvironment } from '../../entities/quickbooks-client/quickbooks-client-environment.enum';

@Injectable()
export class QuickBooksConnectionService {
  constructor(private readonly http: HttpService) {}

  getOpenIdConfig(environment: QuickBooksEnvironment) {
    let url = QUICKBOOKS_STAGING_OIDC_CONFIG;
    if (environment === QuickBooksEnvironment.PRODUCTION) {
      url = QUICKBOOKS_PRODUCTION_OIDC_CONFIG;
    }
    return this.http.get(url);
  }

  getAuthorizationUrl() {
    return QUICKBOOKS_AUTHORIZATION_URL;
  }

  getTokenUrl() {
    return QUICKBOOKS_TOKEN_URL;
  }

  getRevocationUrl() {
    return QUICKBOOKS_REVOCATION_URL;
  }

  getBaseUrl(environment: QuickBooksEnvironment) {
    if (environment === QuickBooksEnvironment.PRODUCTION) {
      return QUICKBOOKS_PRODUCTION_URL;
    } else if (environment === QuickBooksEnvironment.SANDBOX) {
      return QUICKBOOKS_SANDBOX_URL;
    }
  }
}
