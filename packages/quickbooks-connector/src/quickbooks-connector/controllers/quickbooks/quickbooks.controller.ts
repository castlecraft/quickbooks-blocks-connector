import {
  Controller,
  Param,
  UseGuards,
  Req,
  Query,
  Post,
  Get,
  Res,
  Body,
  UsePipes,
  ValidationPipe,
  All,
  Headers,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiBearerAuth } from '@nestjs/swagger';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { QuickBooksClientDto } from '../../policies/quickbooks-client-dto/quickbooks-client.dto';
import { UpdateQuickBooksClientDto } from '../../policies/update-quickbooks-client-dto/update-quickbooks-client.dto';
import { AddQuickBooksClientCommand } from '../../commands/add-quickbooks-client/add-quickbooks-client.command';
import { RemoveQuickBooksClientCommand } from '../../commands/remove-quickbooks-client/remove-quickbooks-client.command';
import { UpdateQuickBooksClientCommand } from '../../commands/update-quickbooks-client/update-quickbooks-client.command';
import { VerifyClientConnectionQuery } from '../../queries/verify-client-connection/verify-client-connection.query';
import { ListQuickBooksClientQuery } from '../../queries/list-quickbooks-client/list-quickbooks-client.query';
import { RetrieveQuickBooksClientQuery } from '../../queries/retrieve-quickbooks-client/retrieve-quickbooks-client.query';
import { RefreshClientTokenCommand } from '../../commands/refresh-client-token/refresh-client-token.command';
import { QuickBooksTokenManagerService } from '../../aggregates/quickbooks-token-manager/quickbooks-token-manager.service';
import { MakeQuickBooksRequestCommand } from '../../commands/make-quickbooks-request/make-quickbooks-request.command';

@Controller('quickbooks')
export class QuickBooksController {
  constructor(
    private readonly tokenManager: QuickBooksTokenManagerService,
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Get('callback')
  callback(
    @Query('code') code,
    @Query('state') state,
    @Query('realmId') realmId,
    @Res() res,
  ) {
    this.tokenManager.processCode(code, state, realmId, res);
  }

  @ApiBearerAuth()
  @Post('v1/connect_client_for_user/:uuid')
  @UseGuards(TokenGuard)
  connectClientForUser(
    @Param('uuid') uuid,
    @Query('redirect') redirect,
    @Req() req,
  ) {
    return this.tokenManager.connectClientForUser(uuid, redirect, req);
  }

  @ApiBearerAuth()
  @Post('v1/connect_client')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ transform: true, whitelist: true }))
  async connectClient(@Body() payload: QuickBooksClientDto) {
    return await this.commandBus.execute(
      new AddQuickBooksClientCommand(payload),
    );
  }

  @ApiBearerAuth()
  @Post('v1/disconnect_client/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, RoleGuard)
  async disconnectClient(@Param('uuid') uuid, @Req() req) {
    const user = req.token.sub;
    return await this.commandBus.execute(
      new RemoveQuickBooksClientCommand(uuid, user),
    );
  }

  @ApiBearerAuth()
  @Post('v1/update_client/:uuid')
  @Roles(ADMINISTRATOR)
  @UsePipes(new ValidationPipe({ transform: true, whitelist: true }))
  @UseGuards(TokenGuard, RoleGuard)
  async updateClient(
    @Param('uuid') uuid,
    @Body() payload: UpdateQuickBooksClientDto,
  ) {
    return await this.commandBus.execute(
      new UpdateQuickBooksClientCommand(uuid, payload),
    );
  }

  @ApiBearerAuth()
  @Get('v1/verify_client_connection/:providerUuid/:realmId')
  @UseGuards(TokenGuard)
  async verifyClientConnection(
    @Param('providerUuid') providerUuid,
    @Param('realmId') realmId,
    @Req() req,
  ) {
    const userUuid = req.token.sub;
    return await this.queryBus.execute(
      new VerifyClientConnectionQuery(userUuid, providerUuid, realmId),
    );
  }

  @ApiBearerAuth()
  @Get('v1/list')
  @UseGuards(TokenGuard)
  async getClientList(
    @Query('offset') offset: number,
    @Query('limit') limit: number,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    return await this.queryBus.execute(
      new ListQuickBooksClientQuery(offset, limit, search, sort),
    );
  }

  @ApiBearerAuth()
  @Get('v1/get/:clientUuid')
  async getQuickBooksClient(@Param('clientUuid') clientUuid) {
    return await this.queryBus.execute(
      new RetrieveQuickBooksClientQuery(clientUuid),
    );
  }

  @ApiBearerAuth()
  @Post('v1/refresh_token_for_client/:clientUuid/:realmId')
  @UseGuards(TokenGuard)
  async refreshTokenForClient(
    @Param('clientUuid') clientUuid,
    @Param('realmId') realmId,
    @Req() req,
  ) {
    const userUuid = req.token.sub;
    await this.commandBus.execute(
      new RefreshClientTokenCommand(clientUuid, userUuid, realmId),
    );
  }

  @ApiBearerAuth()
  @All('v1/command/:clientUuid/*')
  @UseGuards(TokenGuard)
  async makeRequest(
    @Param('clientUuid') clientUuid,
    @Param() params,
    @Query() query,
    @Body() payload,
    @Req() req,
    @Headers() headers,
  ) {
    const userUuid = req.token.sub;
    return await this.commandBus.execute(
      new MakeQuickBooksRequestCommand(
        headers,
        req.method,
        clientUuid,
        params,
        query,
        payload,
        userUuid,
        params[0],
      ),
    );
  }
}
