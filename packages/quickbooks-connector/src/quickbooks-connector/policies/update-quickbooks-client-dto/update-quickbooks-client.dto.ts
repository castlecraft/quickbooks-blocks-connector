import { IsString, IsNotEmpty, IsArray, IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateQuickBooksClientDto {
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty({
    description:
      'Human readable name of client_id registered on QuickBooks instance',
    type: 'string',
    required: true,
  })
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty({
    description: 'registered client_id on QuickBooksClient instance',
    type: 'string',
    required: true,
  })
  clientId: string;

  @IsString()
  @IsOptional()
  @ApiModelProperty({
    description:
      'client_secret of registered client on QuickBooksClient instance',
    type: 'string',
    required: false,
  })
  clientSecret: string;

  @IsArray()
  @IsNotEmpty({ each: true })
  @ApiModelProperty({
    description:
      'String Array of scope allowed for client registered on QuickBooksClient instance',
    isArray: true,
    type: 'string',
    required: true,
  })
  scope: string[];
}
