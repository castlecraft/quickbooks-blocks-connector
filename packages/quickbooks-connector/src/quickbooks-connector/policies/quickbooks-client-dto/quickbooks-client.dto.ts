import { IsString, IsNotEmpty, IsArray } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class QuickBooksClientDto {
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty({
    description:
      'Human readable name of client_id registered on QuickBooks instance',
    type: 'string',
    required: true,
  })
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty({
    description: 'registered client_id on QuickBooks instance',
    type: 'string',
    required: true,
  })
  clientId: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty({
    description: 'client_secret of registered client on QuickBooks instance',
    type: 'string',
    required: true,
  })
  clientSecret: string;

  @IsArray()
  @IsNotEmpty({ each: true })
  @ApiModelProperty({
    description:
      'String Array of scope allowed for client registered on QuickBooks instance',
    isArray: true,
    type: 'string',
    required: true,
  })
  scope: string[];
}
