import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { RequestLogManagerService } from '../../aggregates/request-log-manager/request-log-manager.service';
import { ListUserRequestLogQuery } from './list-user-request-log.query';

@QueryHandler(ListUserRequestLogQuery)
export class ListRequestUserLogHandler
  implements IQueryHandler<ListUserRequestLogQuery> {
  constructor(private manager: RequestLogManagerService) {}

  async execute(query: ListUserRequestLogQuery) {
    const { offset, limit, uuid, search, sort } = query;
    return await this.manager.getUserList(offset, limit, uuid, search, sort);
  }
}
