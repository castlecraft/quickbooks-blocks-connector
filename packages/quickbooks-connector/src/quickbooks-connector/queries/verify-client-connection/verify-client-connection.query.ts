import { IQuery } from '@nestjs/cqrs';

export class VerifyClientConnectionQuery implements IQuery {
  constructor(
    public readonly userUuid: string,
    public readonly providerUuid: string,
    public readonly realmId: string,
  ) {}
}
