import { IQuery } from '@nestjs/cqrs';

export class RetrieveRequestLogQuery implements IQuery {
  constructor(public readonly uuid: string) {}
}
