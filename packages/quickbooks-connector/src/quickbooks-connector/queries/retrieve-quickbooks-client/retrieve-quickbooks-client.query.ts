import { IQuery } from '@nestjs/cqrs';

export class RetrieveQuickBooksClientQuery implements IQuery {
  constructor(public readonly uuid: string) {}
}
