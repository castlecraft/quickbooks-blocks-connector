import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingComponent } from './shared-ui/listing/listing.component';
import { AuthGuard } from './common/guards/auth-guard/auth.guard.service';
import { HomeComponent } from './shared-ui/home/home.component';
import { DashboardComponent } from './shared-ui/dashboard/dashboard.component';
import { QuickBooksClientComponent } from './quickbooks-connector-ui/quickbooks-client/quickbooks-client.component';
import { RequestLogComponent } from './quickbooks-connector-ui/request-log/request-log.component';
import { QuickBooksConnectorSettingsComponent } from './quickbooks-connector-ui/quickbooks-connector-settings';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'dashboard', component: DashboardComponent },

  {
    path: 'list/quickbooks',
    component: ListingComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'list/request_log',
    component: ListingComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'form/quickbooks/:id',
    component: QuickBooksClientComponent,
    canActivateChild: [AuthGuard],
  },
  {
    path: 'form/request_log/:id',
    component: RequestLogComponent,
    canActivateChild: [AuthGuard],
  },
  {
    path: 'settings',
    component: QuickBooksConnectorSettingsComponent,
    canActivate: [AuthGuard],
  },

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
