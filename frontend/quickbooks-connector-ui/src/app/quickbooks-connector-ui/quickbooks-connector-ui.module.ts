import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuickBooksConnectorSettingsComponent } from './quickbooks-connector-settings/quickbooks-connector-settings.component';
import { QuickBooksClientComponent } from './quickbooks-client/quickbooks-client.component';
import { RequestLogComponent } from './request-log/request-log.component';
import { QuickBooksConnectorSettingsService } from './quickbooks-connector-settings/quickbooks-connector-settings.service';
import { SharedImportsModule } from '../shared-imports/shared-imports.module';
import { QuickBooksClientService } from './quickbooks-client/quickbooks-client.service';
import { RequestLogService } from './request-log/request-log.service';

@NgModule({
  declarations: [
    QuickBooksConnectorSettingsComponent,
    QuickBooksClientComponent,
    RequestLogComponent,
  ],
  providers: [
    QuickBooksConnectorSettingsService,
    QuickBooksClientService,
    RequestLogService,
  ],
  imports: [CommonModule, SharedImportsModule],
})
export class QuickBooksConnectorUIModule {}
