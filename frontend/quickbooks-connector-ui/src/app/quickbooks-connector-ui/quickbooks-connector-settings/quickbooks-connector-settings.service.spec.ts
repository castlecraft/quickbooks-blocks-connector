import { TestBed } from '@angular/core/testing';

import { QuickBooksConnectorSettingsService } from './quickbooks-connector-settings.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OAuthService } from 'angular-oauth2-oidc';
import { oauthServiceStub } from '../../common/testing-helpers';

describe('QuickBooksConnectorSettingsService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: QuickBooksConnectorSettingsService = TestBed.get(
      QuickBooksConnectorSettingsService,
    );
    expect(service).toBeTruthy();
  });
});
