#!/bin/bash

cd /tmp

# Clone building blocks configuration
git clone https://github.com/castlecraft/helm-charts

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_FC=$(helm ls -q quickbooks-connector-staging --tiller-namespace staging)
if [ "$CHECK_FC" = "quickbooks-connector-staging" ]
then
    echo "Updating existing quickbooks-connector-staging . . ."
    helm upgrade quickbooks-connector-staging \
        --tiller-namespace staging \
        --namespace staging \
        --reuse-values \
        helm-charts/quickbooks-blocks-connector
fi
